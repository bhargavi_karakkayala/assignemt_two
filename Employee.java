package greatLearning.assignment2;

public class Employee implements Comparable<Employee> {
	int id;
	String name;
	int age;
	int salary;//per annum
	String department;
	String city;
	public Employee(int id, String name, int age, int salary, String department, String city) {
		super();
		try {
			this.id = id;
			if(id<0) {
				throw new IllegalArgumentException("Employee Id should not lessthan or equal to zero");
			}
			this.name = name;
			if(name==null || name.isEmpty()) {
				throw new IllegalArgumentException("Employee Name should not be Empty or Null");
			}
			this.age = age;
			if(age < 0) {
				throw new IllegalArgumentException("Employee Age must not less than or  equal to zero");
			}
			this.salary = salary;
			if(salary < 0) {
				throw new IllegalArgumentException("Employee Salary should not to be less than or equal to zero");
			}
			this.department = department;
			if(department == null || department.isEmpty()) {
				throw new IllegalArgumentException("Employee Department should not to be Empty or Null");
			}
			this.city = city;
			if(city==null || city.isEmpty()) {
				throw new IllegalArgumentException("Employee City should not to be Empty or Null");
			}
		}catch(Exception e) {
			System.out.println(" "+ e.getMessage());
		}
	}
	public String getName() {
		return name;
	}
	public String setName() {
		return this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "" + name + "";
	}
	@Override
	public int  compareTo(Employee o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static Object stream() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
}
