package greatLearning.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		Set<Employee> s1 = new HashSet<>(employees);
		for(Employee e : s1) {
		System.out.println(e.city+"="+ Collections.frequency(employees, e));
		}
	}
	public void monthlySalary(ArrayList<Employee> employees) {
		Collections.sort(employees);
		for(Employee e : employees) {
			System.out.print(e.id+"="+ (double)Math.round(e.salary/12) +", ");
		}
	}

}
