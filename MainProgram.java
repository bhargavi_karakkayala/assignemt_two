package greatLearning.assignment2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

public class MainProgram  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> employees = new ArrayList<>();
			//creating employees list of collections
			Employee e1 = new Employee(1, "Aman", 20,  1100000,"IT", "Delhi");
			Employee e2 = new Employee(2, "Bobby",22,500000,"HR", "Bombay");
			Employee e3 = new Employee(3,  "Zoe", 20, 7500000, "Admin", "Delhi");
			Employee e4 = new Employee(4, "Smitha",21, 1000000,"IT", "Chennai");
			Employee e5 = new Employee(5, "Smitha", 24, 1200000,"HR", "Bengaluru");
			employees.add(e1); employees.add(e2); employees.add(e3); employees.add(e4); employees.add(e5);
		
			//employees list 
			System.out.println("List of employees ");
			System.out.println(" S.No \tName \tAge \tSalary \t\tDepartment \tCity");
			for(Employee e : employees) {
				System.out.println(" "+e.id +"\t"+e.name +"\t"+e.age +"\t"+e.salary +"\t\t"+e.department +"\t\t"+e.city);
			}
		
			//sorting employees names 
			System.out.println("\nNames of all employees in the sorted order are:");
			Collections.sort(employees, new DataStructureA());
			System.out.println(employees);
			
			//city count 
			System.out.println("\nCount of Employees from each city:");
			Set<Employee> s1 = new HashSet<>(employees);
			for(Employee e : s1) {
			System.out.println(e.city+"="+ Collections.frequency(employees, e));
			}
	
			//monthly salary of employee with their id
			System.out.println("\nMonthly Salary of employee along with their ID is:");
			Collections.sort(employees);
			for(Employee e : employees) {
				System.out.print(e.id+"="+ (double)Math.round(e.salary/12) +", ");
			}
		
		}

	}
